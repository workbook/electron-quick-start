// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

const electron = require("electron")
const fs = require("fs")

// 网络
const {net} = require('electron').remote
function accessBaidu(){
  const request = net.request("https://www.baidu.com")
  request.on('response', response => {
    console.log(`**statusCode:${response.statusCode}`)
    console.log(`**headers:${JSON.stringify(response.headers)}`)
    response.on('data', chunk => {
      console.log("接收到的数据：",chunk.toString())
    })
    response.on('end', () => {
      console.log('数据接收完成')
    })
  })
  request.end()
}

// 弹出菜单
// const {remote} = require("electron")
// const {Menu, MenuItem} = remote
// function openMenu(){
//   const template = [
//     {label: "第一个菜单"},
//     {label: "点击测试", click: () => {
//       console.log("点击事件OK")
//     }},
//     {role: "undo"},
//     {role: "redo"},
//     {label: "旅游", type: "checkbox", checked: true},
//     {label: "吃", type: "checkbox", checked: true},
//     {label: "逛街", type: "checkbox", checked: false},
//     new MenuItem({label: "我是menuItem生成的菜单" ,click: () => {
//       console.log("您点击了menuItem的菜单")
//     }}),
//     {
//       label: "子菜单测试",
//       submenu: [
//         {label: "子菜单-1"},
//         {label: "子菜单-2"},
//         {label: "子菜单-3"}
//       ]
//     }
//   ]
//   const menu = Menu.buildFromTemplate(template)
//   // Menu.setApplicationMenu(menu) 这段是改变上面的横栏的菜单
//   menu.popup()
// }
// 进程通信
// const {ipcRenderer} = require("electron");
// function sendMessageToMain(){
//   ipcRenderer.send("send-message-to-main-test", "这是来自于渲染进程的数据666")
// }
// ipcRenderer.on("send-message-to-renderer-test", (event, args) => {
//   console.log("渲染进程接收到的数据", args)
// })
// function sendMessageToMainAync(){
//   console.log(ipcRenderer.sendSync('sync-message','ping'))
// }

// 在渲染进程注册快捷键
// const {remote} = require("electron")
// remote.globalShortcut.register("CommandOrControl+G", () => {
//   console.log("您按下了Ctrl + G");
// })

// dialog
// const { dialog } = require('electron').remote
// function openDialog(){
//   dialog.showOpenDialog({
//     title: "请选择你喜欢的文件",
//     buttonLabel: "走你",
//     filters: [
//       { name: 'Custom File Type', extensions: ['js', 'html', 'json']}
//     ]
//   }).then(result => {
//     console.log(result)
//   }).catch(err => {
//     console.log(err)
//   })
// }

// function saveDialog(){
//   dialog.showSaveDialog({
//     title: "请选择要保存的文件名",
//     buttonLabel: "保存",
//     filters: [
//       { name: 'Custom File Type', extensions: ['js', 'html', 'json']}
//     ]
//   }).then(result => {
//     console.log(result)
//     fs.writeFileSync(result.filePath, "保存文件测试！！！")
//   }).catch(err => {
//     console.log(err)
//   })
// }

// function messageDialog(){
//   dialog.showMessageBox({
//     type: "warning",
//     title: "您确定吗？",
//     message: "您真的想要删除这条数据么？",
//     buttons: ["OK", "Cancel"],
//   }).then(result => {
//     console.log(result)
//   }).catch(err => {
//     console.log(err)
//   })
// }


// window-open
// let subWin;
// function openNewWindow(){
//   // window.open('https://www.baidu.com','baidu')
//   subWin = window.open('popup_page.html','popup')
// }
// function closeWindow(){
//   subWin.close();
// }
// window.addEventListener("message",(msg)=>{
//   console.log('接收到的消息',msg)
// })


// webview 实例
// const wb = document.querySelector('#wb');
// const loading = document.querySelector("#loading")
// wb.addEventListener("did-start-loading", () => {
//   console.log("loading...")
//   loading.innerHTML = "loading...";
// })
// wb.addEventListener("did-stop-loading", ()=>{
//   console.log("OK.")
//   loading.innerHTML = "OK.";
//   // 插入CSS样式
//   wb.insertCSS(`
//     #su {
//       background: red !important;
//     }
//   `)
//   // // 插入JS脚本
//   // wb.executeJavaScript(`
//   //     alert(document.getElementById('su').value)
//   // `)
//   // wb.openDevTools() // 打开webview里面网页的控制台
// })



// 进程
// function getProcessInfo(){
//   console.log('getCPUUsage', process.getCPUUsage())
//   console.log('env', process.env)
//   console.log('arch', process.arch)
// }

// File对象 实例
// const dragWarpper = document.getElementById("drag_test");
// dragWarpper.addEventListener("drop", (e)=>{
//   e.preventDefault();
//   const files = e.dataTransfer.files;
//   if (files && files.length > 0) {
//     const path =  files[0].path;
//     console.log('path:', path)
//     const content = fs.readFileSync(path);
//     console.log(content.toString())
//   }
// })
// dragWarpper.addEventListener("dragover", (e) => {
//   e.preventDefault();
// })