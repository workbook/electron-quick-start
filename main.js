// Modules to control application life and create native browser window
const {app, BrowserWindow, BrowserView, globalShortcut, ipcMain, Menu} = require('electron')
const path = require('path')

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    // frame: false, // 不显示菜单
    show: false, // 先不显示，加载完成了之后再显示
    // backgroundColor: '#ff0000', // 背景颜色
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      webviewTag: true,
      nodeIntegration: true,
      enableRemoteModule: true
    }
  })

  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })

  // 子窗口
  // childWin = new BrowserWindow({
  //   parent: mainWindow,
  //   x: 0, // 坐标
  //   y: 0
  // })

  // 模态窗口，只能操作这个窗口，可能用来做确认框
  // childWin = new BrowserWindow({
  //   parent: mainWindow,
  //   modal: true
  // })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // const view = new BrowserView();
  // view.setBounds({
  //   x: 10,
  //   y: 10,
  //   width: 300,
  //   height: 200
  // })

  // view.webContents.loadURL('https://www.baidu.com')
  // mainWindow.setBrowserView(view)

  // setTimeout(()=>{
  //   view.destroy();
  // }, 5000);

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  mainWindow.webContents.on('did-finish-load', ()=>{
    console.log('####did-finish-load')
  })

  mainWindow.webContents.on('dom-ready', ()=>{
    console.log('####dom-ready')
  })

  // 主进程主动和渲染进程通信
  // setTimeout(() => {
  //   mainWindow.webContents.send("send-message-to-renderer-test", "我是主进程，我主动和你搭讪")
  // }, 5000)

  // 主进程中弹菜单，主进程很少有需求需要弹出菜单
  // setTimeout(() => {
  //   const template = [
  //     {label: "第一个菜单"},
  //     {label: "第二个菜单"},
  //     {role: "undo"},
  //     {type: "separator"},
  //     {label: "第三个菜单"},
  //     {label: "第四个菜单"}
  //   ]
  //   const menu = Menu.buildFromTemplate(template)
  //   // Menu.setApplicationMenu(menu) 这段是改变上面的横栏的菜单
  //   menu.popup()
  // }, 2000)
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  console.log('####ready')
  createWindow()
  
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })

  // 注册一个'CommandOrControl+X'的全局快捷键
  // const ret = globalShortcut.register('CommandOrControl+X', () => {
  //   console.log('CommandOrControl+X is pressed')
  // })

  // if(!ret){
  //   console.log('registration failed')
  // }

  // // 检查快捷键是否注册成功
  // console.log(globalShortcut.isRegistered('CommandOrControl+X'))
})

// app.on('will-quit', () => {
//   // 注销快捷键
//   globalShortcut.unregister('CommandOrControl+X')

//   // 注销所有快捷键
//   globalShortcut.unregisterAll()
// })

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  console.log('####window-all-closed')
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

// ipcMain.on("send-message-to-main-test", (event, args) => {
//   console.log("主进程接收到的数据是：", args)
//   event.reply("send-message-to-renderer-test", "这是来自于主进程的问候")
// })

// ipcMain.on("sync-message", (event, args) =>{
//   console.log("data: ", args)
//   event.returnValue = 'pong'
// })
